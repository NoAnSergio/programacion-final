#ifndef ENEMY2_H
#define ENEMY2_H

#include "Entity.h"

class Enemy2:public Entity
{
	public:
		Enemy2();
		~Enemy2();

		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render();
		virtual void update();

		void updateControls();

		bool isOfClass(std::string classType);
		std::string getClassName() { return "Enemy2"; };

	protected:
	
	private:
	
};

#endif
