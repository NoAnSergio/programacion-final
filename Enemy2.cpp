//Include our classes
#include "Enemy2.h"
#include "singletons.h"

Enemy2::Enemy2(){
	life = 200;

}

Enemy2::~Enemy2(){
}

void Enemy2::init() {
	Entity::init();
}
void Enemy2::init(int x, int y) {
	Entity::init(x, y);
}
void Enemy2::init(int graphic, int x, int y, int w, int h) {
	Entity::init(graphic, x, y, w, h);
}

void Enemy2::update(){
	Entity::render();

}
void Enemy2::render(){
	Entity::update();

}

void Enemy2::updateControls() {
	return;
}

bool Enemy2::isOfClass(std::string classType){
	if (classType == "Enemy2" ||
		classType == "Entity") {
		return true;
	}
	return false;
}

