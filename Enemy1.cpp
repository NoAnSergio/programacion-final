//Include our classes
#include "Enemy1.h"
#include "singletons.h"


Enemy1::Enemy1() : Entity(){
mpSpeed = 150;
life = 200;
stopMove = 0;
}

Enemy1::~Enemy1(){
}

void Enemy1::init(){
	Entity::init();
}
void Enemy1::init(int x, int y){
	Entity::init(x, y);
}
void Enemy1::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);
}

void Enemy1::render() {
	Entity::render();
}

void Enemy1::update() {
	Entity::update();

}

void Enemy1::updateControls() {

	if (mCooldownMove > 0) {
		mCooldownMove -= global_delta_time;
		return;
	}
	if (stopMove == 3){
		mpDirection = NONE;
		mCooldownMove = 500;
		stopMove = 0;
		return;
	}
	if (mCooldownMove <= 0) {

		int randomDir = rand() % 4;
		if (randomDir == 0) {
			mpDirection = UP;
			stopMove++;
		}
		if (randomDir == 1) {
			mpDirection = RIGHT;
			stopMove++;
		}
		if (randomDir == 2) {
			mpDirection = DOWN;
			stopMove++;
		}
		if (randomDir == 3) {
			mpDirection = LEFT;
			stopMove++;
		}
		
		mCooldownMove = 250;
	}
	return;
}

bool Enemy1::isOfClass(std::string classType){
	if(classType == "Enemy1" || 
		classType == "Entity"){
		return true;
	}
	return false;
}

