#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include "includes.h"
#include "ofMain.h"
#include <map>
#include <vector>

//! ResourceManager class
/*!
	Handles the load and management of the graphics in the game.
*/
class ResourceManager
{
	public:
		//! Constructor of an empty ResourceManager.
		ResourceManager();

		//! Destructor.
		~ResourceManager();

		//! Gets the graphic ID
		/*!
			\param file Filepath to the graphic
			\return ID of the graphic
		*/
		int getGraphicID(const char* file);

		//! Gets the graphic path given an ID
		/*!
			\param ID of the graphic
			\return Filepath to the graphic
		*/
		std::string getGraphicPathByID(int ID);

		//! Returns width and Height of a Texture
		/*!
		 *	param img ID texture
		 *	param width Return variable for width value
		 *	param height Return variable for height value
		 */
		void getGraphicSize(int img, int &width, int &height);

		//! Returns width and Height of a Texture
		/*!
		 *	param img ID texture
		 *	param width Return variable for width value
		 *	param height Return variable for height value
		 */
		int getGraphicWidth(int img);
		int getGraphicHeight(int img);

		//! Returns the ofImage of the graphic
		/*!
			\param ID ID of the graphic
			\return ofImage
		*/
		ofImage* getGraphicByID(int ID);

		//! Prints the path to loaded graphics
		void printLoadedGraphics();

		//! Returns the ClassName of the object
		/*!
			\return ClassName (as a string)
		*/
		std::string getClassName(){return "ResourceManager";};

		//! Gets Singleton instance
		/*!
			\return Instance of ResourceManager (Singleton).
		*/
		static ResourceManager* getInstance();

	private:

		//! Adds a graphic to the ResourceManager
		/*!
			\param file Filepath to the graphic
			\return -1 if there's an error when loading
		*/
		int addGraphic(const char* file);

		//! Searches in the ResourceManager and gets the graphic by its name. If it isn't there, loads it
		/*!
			\param file Filepath of the graphic
			\return ofImage
		*/
		ofImage* getGraphic(const char* file);

		//! Searches the graphic in the vector and returns its ID
		/*!
			\param img ofImage of the graphic
			\return ID of the graphic
		*/
		int searchGraphic(ofImage* img);

		//! Searches the first NULL in mGraphicsVector and updates mFirstFreeSlot to store its position
		/*!
			\return Index of the first NULL in mGraphicsVector
		*/
		int updateFirstFreeSlot();

		std::map<std::string, ofImage*> 	mGraphicsMap;	/*!<  Map that stores Surfaces. Useful for direct access*/
		std::vector<ofImage*>				mGraphicsVector;/*!<  Vector that stores Surfaces. Useful for sequential access*/
		std::map<std::string, int>			mIDMap;			/*!<  Map that stores ID. Links strings to ID, useful for sequential access*/

		int									mFirstFreeSlot;	/*!<  First free slot in the mGraphicsVector*/
		static ResourceManager*				pInstance;		/*!<  Singleton instance*/
};

#endif
