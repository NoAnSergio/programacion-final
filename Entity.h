#ifndef ENTITY_H
#define ENTITY_H

#include "ofMain.h"
#include "includes.h"
#include "Renderer.h"
#include "Utils.h"

class Entity
{
	public:
		Entity();
		~Entity();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);
		virtual void render(int offX = 0, int offY = 0);
		virtual void update();

		void setX(int x);
		void setY(int y);
		void setXY(int x, int y);
		
		void setW(int w);
		void setH(int h);
		void setRectangle(C_Rectangle rect);
		void setRectangle(int x, int y, int w, int h);

		bool getAlive() { return mpAlive; };
		void setAlive(bool alive);

		bool isInsideRectangle(C_Rectangle a_rect);

		int getX(){return mpRect.x;};
		int getY(){return mpRect.y;};
		int getW(){return mpRect.w;};
		int getH(){return mpRect.h;};
		
		C_Rectangle getRect() { return mpRect; };

		bool isOfClass(std::string classType);
		std::string getClassName(){return "Entity";};

		//
		void setdamage(int damage);
		int getLife() { return life; };
		int getdamage() { return damage; }
		void setLoadLife(int mlife);

	protected:
		virtual void updateControls();
		bool checkCollisionWithMap();
		bool checkCollisionLine(int x1, int y1, int x2, int y2, int dp);
		void move();

		virtual void updateGraphic();

		C_Rectangle 	mpRect;	//Collision and Position
		C_Rectangle 	mpGraphicRect;
		int				mpGraphicImg;

		int				mFrame;
		int				mMaxFrame;
		int				mCurrentFrameTime;
		int				mMaxFrameTime;
	
		bool 			mpAlive;

		int				mpDirection;
		int				mpSpeed;
		bool			mpMoving;

		int				mpXtoGo;
		int				mpYtoGo;

		int				mpInitialX;
		int				mpInitialY;
		
		bool			mpTileBasedMovement;

		//
		int life;
		int damage;
};

#endif
