#ifndef ENEMY1_H
#define ENEMY1_H

#include "Entity.h"

class Enemy1 : public Entity
{
	public:
		Enemy1();
		~Enemy1();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render();
		virtual void update();

		void updateControls();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "Enemy1";};

	protected:

		int mCooldownMove;
		int stopMove;
};

#endif
