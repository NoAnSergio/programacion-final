#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "includes.h"
#include "Player.h"
//#include "Entity.h"

class GameState
{
	public:
		static GameState* getInstance();
		void guardarPartida();
		void cargarPartida();
		void init();
		void update();
		//
		void setPlayer(Player* loadPlayer);

	protected:
		Player* mpPlayer;

		int PlayerX;
		int PlayerY;
		int Playerlife;

		static GameState* pInstance;		/*!<  Singleton instance*/
	
};

#endif
