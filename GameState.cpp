//Include our classes
#include "GameState.h"

GameState* GameState::pInstance = NULL;

GameState* GameState::getInstance() {
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new GameState();
	}
	return pInstance;
	
}

void GameState::init() {
	
}

void GameState::update() {

}


//
void GameState::setPlayer(Player* pPlayer) {
	mpPlayer = pPlayer;
}
//

void GameState::guardarPartida() {

	std::fstream save;
	save.open("save.txt", std::ios::out | std::ios::binary | std::ios::trunc);

	// character
	PlayerX = mpPlayer->getX();
	PlayerY = mpPlayer->getY();
	Playerlife = mpPlayer->getLife();

	save.write((char*)&PlayerX, sizeof(int));
	save.write((char*)&PlayerY, sizeof(int));
	save.write((char*)&Playerlife, sizeof(int));

	save.close();

}

void GameState::cargarPartida() {

	std::fstream load;
	load.open("save.txt", std::ios::in | std::ios::binary);

	// character
	load.read((char*)&PlayerX, sizeof(int));
	load.read((char*)&PlayerY, sizeof(int));
	load.read((char*)&Playerlife, sizeof(int));

	mpPlayer->setXY(PlayerX, PlayerY);
	mpPlayer->setLoadLife(Playerlife);

	load.close();
}